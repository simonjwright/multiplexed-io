# multiplexed-io

This contains demonstrations, for
[AdaPilot](http://adapilot.likeabird.eu), of various methods of doing
multiplexed I/O for an ARM Cortex-M MCU, using the Ravenscar profile
of Ada 2012 from [AdaCore](http://libre.adacore.com).